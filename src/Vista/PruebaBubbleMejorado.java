/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.MatrizPalabra;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author USUARIO
 */
public class PruebaBubbleMejorado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://raw.githubusercontent.com/javierarce/palabras/master/listado-general.txt");
        Object lineas[]=archivo.leerArchivo();
        int canLineas=lineas.length;
        MatrizPalabra m=new MatrizPalabra(lineas,1000);
        m.bubbleMejor();
        MatrizPalabra m1=new MatrizPalabra(lineas,5000);
        m1.bubbleMejor();
        MatrizPalabra m2=new MatrizPalabra(lineas,10000);
        m2.bubbleMejor();
        MatrizPalabra m3=new MatrizPalabra(lineas,20000);
        m3.bubbleMejor();
        MatrizPalabra m4=new MatrizPalabra(lineas,30000);
        m4.bubbleMejor();
        MatrizPalabra m5=new MatrizPalabra(lineas,80000);
        m5.bubbleMejor();
        
        //System.out.println(m.toString());
        
    }
    
}
